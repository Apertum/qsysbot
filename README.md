QSYSTEM BOT<br>
<b>Plugin for Telegram.</b>

RU<br>
Данный плагин предназначен для рассылки событий хода очереди в общий чат телеграм-ботом. Некий аналог главного табло с расширенным набором событий для вывода на экран чата. 

Что нужно сделать, чтобы подулючить бота к чату:
1. Поместить файл плагина qsysbot.jar в папку plugins. Файл настроек qsysbot.properties поместить в папку config\qsysbot\ или config\, даже в корень QSystem сойдёт.
2. Регистрируем бота в BotFather, получаем его ID вида 1234567890:xxxxxXxxxxxXXxxxXXxxxXXxxxxxx
3. Регистрируем группу в Телеграмм. Добавляем в группу бота и делаем его админом группы с доступом к сообщениям. Другим членам группы не админам лучше запретить писать в чат группы.
4. Настраиваем рассылку в qsysbot.properties. Как настраивать смотрите в описании настроек в файле.
5. Готово. Теперь QSystem будет слать сообщения в группы согласно настройкам.

EN<br>
This plugin is intended for sending queue progress events to the general chat as a telegram bot. A kind of analogue of the main board with an extended set of events for displaying a chat.

What you need to do to connect the bot to the chat:
1. Place the plugin file qsysbot.jar in the plugins folder. Place the qsysbot.properties configuration file in the config\qsysbot\ or config\folder, even in the QSystem root it will work.
2. Register a bot in BotFather, get its ID of the form 1234567890:xxxxxXxxxxxXXxxxXXxxxXXxxxxxx
3. Register the group in Telegram. Add the bot to the group and make it a group admin with access to messages. It is better for other non-admin members of the group to be prohibited from writing to the group chat.
4. Configure qsysbot.properties. How to configure, see the description of the settings in the file.
5. Done. Now QSystem will send messages to groups according to the settings.