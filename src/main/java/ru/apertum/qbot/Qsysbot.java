package ru.apertum.qbot;


import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import ru.apertum.qbot.core.BotException;
import ru.apertum.qbot.core.Requester;
import ru.apertum.qbot.core.Rotator;
import ru.apertum.qbot.core.SevereException;
import ru.apertum.qbot.dto.SendMsgRs;
import ru.apertum.qbot.dto.TestRs;
import ru.apertum.qbot.plugin.Event;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

@Log4j2
@Getter
public class Qsysbot {

    private static final String CONFIG_FILE = "config/qsysbot.properties";
    private static final String CONFIG_FILE1 = "config/qsysbot/qsysbot.properties";
    private static final String CONFIG_FILE2 = "qsysbot.properties";
    private String urlBase;
    private Requester requester;

    private String[] chatIdList = new String[]{};
    private List<Integer> events = new ArrayList<>();
    private Map<String, List<Integer>> chatEvents = new HashMap<>();
    private Map<String, List<Long>> chatServiceIds = new HashMap<>();
    private Mustache msgMustache;

    public static void main(String[] args) {
        log.info("Start BOT for QSystem...");
        final Qsysbot bot = new Qsysbot();
        final TestRs testRs = bot.init(bot.getConfigFile(args.length > 0 ? args[0] : null));
        if (testRs != null && testRs.isStatus()) {
            log.info("Starting...");
            bot.run();
        } else {
            log.warn("Test Telegram for Qsysbot is not successful.");
        }
        log.info("...FINISH BOT for QSystem");
    }

    public String getConfigFile(String defName) {
        if (defName != null && Files.exists(Paths.get(defName))) {
            return defName;
        }
        return Files.exists(Paths.get(CONFIG_FILE1)) ? CONFIG_FILE1 : (Files.exists(Paths.get(CONFIG_FILE)) ? CONFIG_FILE : CONFIG_FILE2);
    }

    public TestRs init(String filename) {
        urlBase = loadProps(filename);
        if (urlBase == null) {
            throw new SevereException("BOT for QSystem was stopped by error. No base url.");
        }
        log.debug("URL base for API: {}", urlBase);
        requester = new Requester(urlBase);
        try {
            return requester.run("getMe", TestRs.class, null);
        } catch (IOException | BotException ex) {
            throw new SevereException(ex);
        }
    }

    public void run() {
        final Rotator rotator = new Rotator(requester);
        rotator.start(true);
    }

    public int sendMessages(Event event) throws BotException {
        boolean res1 = false;
        boolean res2 = false;
        for (String chatId : chatIdList) {
            if (getChatEvents().get(chatId) != null && !getChatEvents().get(chatId).contains(event.getState().ordinal())) {
                continue;
            }
            if (getChatEvents().get(chatId) == null && !getEvents().contains(event.getState().ordinal())) {
                continue;
            }
            if (getChatServiceIds().get(chatId) != null && !getChatServiceIds().get(chatId).contains(event.getCustomer().getService().getId())) {
                continue;
            }
            final SendMsgRs msgRs = sendMessage(chatId, event.createMessage(chatId, msgMustache));
            if (msgRs.isStatus()) {
                res1 = true;
            } else {
                res2 = true;
                log.error("Send message was failed. chatId={}, event={}; code={}; description: {}", chatId, event, msgRs.getErrorCode(), msgRs.getDescription());
            }
        }
        return res1 && res2 ? 2 : (res1 ? 1 : 0);
    }

    public SendMsgRs sendMessage(String chatId, String message) throws BotException {
        try {
            //1строка%0A2строка   %0A-перенос строки
            SendMsgRs sendMsgRs = requester.run("sendMessage", SendMsgRs.class, message);
            log.trace("Send msg. status={}; chatId={}, message={}", sendMsgRs.isStatus(), chatId, message);
            return sendMsgRs;
        } catch (IOException ex) {
            throw new SevereException(ex);
        }
    }

    private String loadProps(String filename) {
        if (Files.notExists(Paths.get(filename))) {
            log.error("File {} not found.", filename);
            return null;
        }
        try (InputStream input = new FileInputStream(filename)) {
            final Properties prop = new Properties();
            InputStreamReader inR = new InputStreamReader(input, StandardCharsets.UTF_8);
            prop.load(inR);
            final String splitter = "\\s*(;|,|\\s)\\s*";
            chatIdList = prop.getProperty("chat_id").split(splitter);
            events = Arrays.stream(prop.getProperty("events").split(splitter)).filter(s -> s.matches("\\d+")).map(Integer::parseInt).collect(Collectors.toList());
            for (String chatId : chatIdList) {
                final String evtseventsForChat = prop.getProperty(chatId);
                if (evtseventsForChat != null && !evtseventsForChat.isEmpty()) {
                    List<Integer> list = Arrays.stream(evtseventsForChat.split(splitter)).filter(s -> s.matches("\\d{1,2}")).filter(s -> s.matches("^\\d+$")).map(Integer::parseInt).collect(Collectors.toList());
                    chatEvents.put(chatId, list);
                }

                final String servs = prop.getProperty(chatId + "-services");
                if (servs != null && !servs.isEmpty()) {
                    List<Long> listSrv = Arrays.stream(servs.split(splitter)).filter(s -> s.matches("-{0,1}\\d{1,15}")).filter(s -> s.matches("^\\d+$")).map(Long::parseLong).collect(Collectors.toList());
                    chatServiceIds.put(chatId, listSrv);
                }
            }
            final String pattern = prop.getProperty("message_pattern");
            msgMustache = new DefaultMustacheFactory().compile(new StringReader(pattern), "telegram_msg");
            urlBase = prop.getProperty("cmd_base_url");
            return urlBase;
        } catch (IOException ex) {
            log.error("Props was not read.", ex);
            return null;
        }
    }
}
