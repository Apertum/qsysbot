package ru.apertum.qbot;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Evgeniy Egorov
 */
public class Version {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println();
        System.out.println("" +
                ".............l}.....................\n" +
                ".............l\\.....................\n" +
                ".............l \\....................\n" +
                "............/l -\\...................\n" +
                ".........../-l ..\\..................\n" +
                "........../_ l ___\\_................\n" +
                ".......\\_/__==_(____--l.............\n" +
                "........\\____________ll\\............\n" +
                ".^.^.^.^.^.^.^.^.^.^.^.^.^.^.^.^.^.");
        System.out.println();
        System.out.println("***       Telegram Plugin       ***");
        System.out.println("     " + description);
        System.out.println("     version " + version);
        System.out.println("     date " + date);
        System.out.println("***      QMS Apertum-QSystem     ***");
        System.out.println();
    }

    static {
        final Properties settings = new Properties();
        final InputStream inStream = settings.getClass().getResourceAsStream("/qsysbot-about.properties");
        try {
            settings.load(inStream);
        } catch (IOException ex) {
            throw new RuntimeException("Cant read version. " + ex);
        }

        version = settings.getProperty("version");
        date = settings.getProperty("date");
        description = String.format(settings.getProperty("description"), settings.getProperty("version"), settings.getProperty("date"), settings.getProperty("UID"));
    }

    public static String version;
    public static String date;
    public static String description;
}
