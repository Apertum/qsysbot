package ru.apertum.qbot.core;

import com.google.gson.Gson;
import lombok.extern.log4j.Log4j2;
import ru.apertum.qbot.dto.BaseRs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@Log4j2
public final class Requester {

    private final String urlBase;

    public Requester(String urlBase) {
        this.urlBase = urlBase;
    }

    public <T extends BaseRs> T run(String command, Class<T> classOfT, String body) throws IOException, BotException {
        final String response = body == null ? doRequest(urlBase + command) : doRequest(urlBase + command, body);
        if (response == null) {
            throw new BotException("Error command=" + command + " for class " + classOfT.getCanonicalName());
        }
        Gson gson = GsonPool.getInstance().borrowGson();
        try {
            final T fromJson = gson.fromJson(response, classOfT);
            if (fromJson.isStatus()) {
                log.debug("GOOD request: {}", fromJson);
            } else {
                log.error("Bad request. code={}; description: {}", fromJson.getErrorCode(), fromJson.getDescription());
            }
            return fromJson;
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
    }

    private String doRequest(String urlStr) throws IOException {
        log.trace("Http request to {}", urlStr);
        URL url = new URL(urlStr);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        con.setConnectTimeout(1000);
        con.setReadTimeout(1000);
        int status = con.getResponseCode();
        if (status == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            return content.toString();
        } else {
            log.error("Error from API. status = {}", status);
            return null;
        }
    }

    private String doRequest(String urlStr, String body) throws IOException {
        log.trace("Http POST to {}", urlStr);
        log.debug("Body:\n{}", body);
        URL url = new URL(urlStr);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        con.setConnectTimeout(1000);
        con.setReadTimeout(1000);
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = body.getBytes(StandardCharsets.UTF_8);
            os.write(input, 0, input.length);
        }
        int status = con.getResponseCode();
        if (status == 200) {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            con.disconnect();
            return content.toString();
        } else {
            log.error("Error from API. status = {}", status);
            return null;
        }
    }
}
