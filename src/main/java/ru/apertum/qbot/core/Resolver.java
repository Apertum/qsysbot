package ru.apertum.qbot.core;

import lombok.Data;
import ru.apertum.qbot.dto.UpdatesRs;

import java.util.LinkedList;
import java.util.List;

public class Resolver {
    // разбор запроса
    // ищем ответ

    public boolean parse(UpdatesRs updatesRs) {
        return false;
    }

    public List<Answer> getAnswer() {
        return new LinkedList<>();
    }

    @Data
    public static class Answer {
        private final String chatId;
        private final String message;
    }
}
