package ru.apertum.qbot.core;

import lombok.extern.log4j.Log4j2;
import ru.apertum.qbot.dto.SendMsgRs;
import ru.apertum.qbot.dto.UpdatesRs;

import java.io.IOException;
import java.util.List;

@Log4j2
public final class Rotator {

    boolean active = false;

    final Requester requester;

    public Rotator(Requester requester) {
        this.requester = requester;
    }

    public void start(boolean makeStart) {
        active = makeStart;
        while (active) {
            boolean hadNew = makeTurn();
            int delta = makeDelta(hadNew);
            waitTo(delta);
            active = false;
        }
    }

    private void waitTo(int delta) {
        try {
            Thread.sleep(delta);
        } catch (InterruptedException e) {
            log.error(e);
            Thread.currentThread().interrupt();
        }
    }

    private int ticker = 0;

    private int makeDelta(boolean hadNew) {
        int delta;
        if (hadNew) {
            delta = 20_000;
            ticker = 0;
        } else {
            ticker++;
            if (ticker > 6) {
                ticker = 6;
            }
            delta = ticker * 10_000;
        }
        return delta;
    }

    private boolean makeTurn() {
        // запрос
        final UpdatesRs updatesRs;
        try {
            updatesRs = requester.run("getUpdates", UpdatesRs.class, null);
        } catch (IOException | BotException ex) {
            throw new SevereException(ex);
        }
        if (updatesRs.isStatus()) {
            log.info("GOOD UPDATES: {}", updatesRs);
        } else {
            log.error("BAD UPDATES. code={}; description: {}", updatesRs.getErrorCode(), updatesRs.getDescription());
            return false;
        }
        // разбор запроса
        Resolver resolver = new Resolver();
        final boolean haveNew = resolver.parse(updatesRs);
        if (haveNew) {
            // ищем ответ
            final List<Resolver.Answer> answers = resolver.getAnswer();
            // отсылаем ответ
            answers.forEach(answer -> {
                try {
                    //1строка%0A2строка   %0A-перенос строки
                    SendMsgRs sendMsgRs = requester.run(String.format("sendMessage?chat_id=%s&text=%s", answer.getChatId(), answer.getMessage()), SendMsgRs.class, null);
                    if (sendMsgRs.isStatus()) {
                        log.info("GOOD UPDATE: id={}", sendMsgRs.getUpdate().getUpdateId());
                    } else {
                        log.error("BAD UPDATE. code={}; description: {}", sendMsgRs.getErrorCode(), sendMsgRs.getDescription());
                    }
                } catch (IOException | BotException ex) {
                    throw new SevereException(ex);
                }
            });
            // результат - были новые сообщения и ответы на него
            return true;
        } else {
            return false;
        }
    }
}
