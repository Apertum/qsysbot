package ru.apertum.qbot.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class BaseRs {

    @Expose
    @SerializedName("ok")
    private boolean status;

    @Expose
    @SerializedName("error_code")
    private Integer errorCode;

    @Expose
    @SerializedName("description")
    private String description;
}
