package ru.apertum.qbot.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SendMsgRs extends BaseRs {

    @Expose
    @SerializedName("result")
    private Update update;
}
