package ru.apertum.qbot.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TestRs extends BaseRs {
    @Expose
    @SerializedName("result")
    private TestResult result;

    @Data
    public static class TestResult {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("is_bot")
        private Boolean isBot;

        @Expose
        @SerializedName("first_name")
        private String firstName;

        @Expose
        @SerializedName("username")
        private String userName;

        @Expose
        @SerializedName("can_join_groups")
        private Boolean canJoinGroups;

        @Expose
        @SerializedName("can_read_all_group_messages")
        private Boolean canReadAllGroupMessages;

        @Expose
        @SerializedName("supports_inline_queries")
        private Boolean isSupportsInlineQueries;
    }
}
