package ru.apertum.qbot.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class Update extends BaseRs {

    @Expose
    @SerializedName("update_id")
    private Long updateId;

    @Expose
    @SerializedName("message")
    private Message message;

    @Data
    public static class Message {

        @Expose
        @SerializedName("message_id")
        private Long messageId;

        @Expose
        @SerializedName("from")
        private From from;

        @Expose
        @SerializedName("chat")
        private Chat chat;

        @Expose
        @SerializedName("sender_chat")
        private Chat senderChat;

        @Expose
        @SerializedName("date")
        private Date date;

        @Expose
        @SerializedName("new_chat_photo")
        private List<ChatPhoto> chatPhotos;

        @Expose
        @SerializedName("group_chat_created")
        private Boolean groupChatCreated;

        @Expose
        @SerializedName("text")
        private String text;

        @Expose
        @SerializedName("entities")
        private List<Entity> entities;

        @Expose
        @SerializedName("migrate_to_chat_id")
        private Long migrateToChatId;
    }

    @Data
    public static class From {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("is_bot")
        private Boolean isBot;

        @Expose
        @SerializedName("first_name")
        private String firstName;

        @Expose
        @SerializedName("username")
        private String username;

        @Expose
        @SerializedName("language_code")
        private String languageCode;
    }

    @Data
    public static class Chat {

        @Expose
        @SerializedName("id")
        private Long id;

        @Expose
        @SerializedName("first_name")
        private String firstName;

        @Expose
        @SerializedName("username")
        private String username;

        @Expose
        @SerializedName("type")
        private String type;

        @Expose
        @SerializedName("title")
        private String title;

        @Expose
        @SerializedName("all_members_are_administrators")
        private Boolean allMembersAreAdministrators;
    }

    @Data
    public static class ChatPhoto {
        @Expose
        @SerializedName("file_id")
        private String fileId;

        @Expose
        @SerializedName("file_unique_id")
        private String fileUniqueId;

        @Expose
        @SerializedName("file_size")
        private Integer fileSize;

        @Expose
        @SerializedName("width")
        private Integer width;

        @Expose
        @SerializedName("height")
        private Integer height;
    }

    @Data
    public static class Entity {

        @Expose
        @SerializedName("offset")
        private Integer offset;

        @Expose
        @SerializedName("length")
        private Integer length;

        @Expose
        @SerializedName("type")
        private String type;
    }
}
