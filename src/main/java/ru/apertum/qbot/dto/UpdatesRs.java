package ru.apertum.qbot.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class UpdatesRs extends BaseRs {

    @Expose
    @SerializedName("result")
    private List<Update> result;
}
