/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qbot.plugin;

import com.github.mustachejava.Mustache;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.apertum.qbot.core.GsonPool;
import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.model.QCustomer;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Evgeniy Egorov
 */
@Data
public class Event {

    private final QCustomer customer;
    private final String point;
    private final String ticket;
    private final CustomerState state;
    private final String preffix;
    private final int number;

    public String createMessage(String chatId, Mustache msgMustache) {
        final String msg;
        if (msgMustache == null) {
            msg = "Номер " + ticket
                    + (state.isReceivingService() ? ("  ->  окно №" + point) : "")
                    + "\n" + state.getState()
                    + "\n" + state.getDescription();
        } else {
            final Map<String, Object> map = new HashMap<>();
            map.put("event", this);
            msg = msgMustache.execute(new StringWriter(), map).toString();
        }

        Gson gson = GsonPool.getInstance().borrowGson();
        try {
            return gson.toJson(new TelegramMessage(chatId, msg));
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
    }

    /**
     * {"chat_id": "@NashaOchered", "text": "Русс яз"}
     */
    @Data
    @NoArgsConstructor
    public static class TelegramMessage {

        @Expose
        @SerializedName("chat_id")
        private String chatId;

        @Expose
        @SerializedName("text")
        private String text;

        @Expose
        @SerializedName("parse_mode")
        private String parseMode = "HTML";

        public TelegramMessage(String chatId, String text) {
            this.chatId = chatId;
            this.text = text;
        }
    }
}
