/*
 *
 * Copyright (C) 2011 Evgeniy Egorov
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ru.apertum.qbot.plugin;

import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IChangeCustomerStateEvent;

/**
 * Плагин во время смены статуса клиенту отсылает статистику в сервер
 * отображений зональных табло по сети
 *
 * @author Evgeniy Egorov
 */
public class EventSender implements IChangeCustomerStateEvent, IBotPluginUID {

    @Override
    public void change(String userPoint, String customerPrefix, int customerNumber, CustomerState cs) {
        // deprecated
    }

    @Override
    public void change(QCustomer qc, CustomerState cs, Long newServiceId) {
        // Создаем событие
        String nom = qc.getPrefix() + qc.getNumber();
        TelegramSender.getInstance().send(new Event(qc, qc.getUser() == null ? "" : qc.getUser().getPoint(), nom, cs, qc.getPrefix(), qc.getNumber()));
    }

    @Override
    public String getDescription() {
        return "Плагин \"QSysBotPlugin\" во время смены статуса клиенту выводит инфу в Telegram";
    }

    @Override
    public long getUID() {
        return UID;
    }
}
