/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.qbot.plugin;

import lombok.extern.log4j.Log4j2;
import ru.apertum.qbot.Qsysbot;
import ru.apertum.qbot.core.BotException;
import ru.apertum.qbot.dto.TestRs;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Создает поток, в потоке порт и им шлет.
 *
 * @author Evgeniy Egorov
 */
@Log4j2
public class TelegramSender {

    public static TelegramSender getInstance() {
        return TelegramSenderHolder.INSTANCE;
    }

    private static class TelegramSenderHolder {

        private static final TelegramSender INSTANCE = new TelegramSender();
    }

    private final LinkedBlockingQueue<Event> stream = new LinkedBlockingQueue<>();
    private final Thread sndThread;
    private final Qsysbot bot;

    private TelegramSender() {
        // поднимим бота
        bot = new Qsysbot();
        final TestRs testRs = bot.init(bot.getConfigFile(null));
        if (testRs.isStatus()) {
            // подготовим поток для отсыла
            sndThread = new Thread(() -> {
                while (!Thread.interrupted()) {
                    final Event event;
                    try {
                        event = stream.take();
                    } catch (InterruptedException ex) {
                        log.error("Interrupted!", ex);
                        Thread.currentThread().interrupt();
                        break;
                    }
                    synchronized (TelegramSenderHolder.INSTANCE) {
                        int res;
                        try {
                            res = bot.sendMessages(event);
                        } catch (BotException e) {
                            res = 3;
                        }
                        switch (res) {
                            case 0: {
                                log.error("Not sent.");
                                break;
                            }
                            case 1: {
                                log.info("Was sent successfully.");
                                break;
                            }
                            case 2: {
                                log.warn("Was sent successfully partially only.");
                                break;
                            }
                            case 3: {
                                log.warn("Something wrong. Bot is mad!");
                                break;
                            }
                            default:
                                throw new IllegalStateException("Result " + res + " is strange.");
                        }
                    }
                }
            });
            sndThread.setDaemon(true);
        } else {
            sndThread = null;
            log.error("Bot for Telegram was not started. code={}, description: {}", testRs.getErrorCode(), testRs.getDescription());
        }
    }

    public void send(Event event) {
        if (sndThread != null) {
            if (sndThread.getState() == Thread.State.NEW || sndThread.getState() == Thread.State.TERMINATED) {
                sndThread.start();
            }
            try {
                stream.put(event);
            } catch (InterruptedException ex) {
                log.error("Interrupted!", ex);
                Thread.currentThread().interrupt();
            }
        } else {
            log.warn("Telegram Bot is not working.");
        }
    }
}
